<table border="1" style="width: 700px;">
    <tr>
        <th>Sl#</th>
        <th>Name</th>
        <th>Price</th>
    </tr>
    @foreach ($products as $product)
    <tr>
        <td>{{ $loop->iteration }}</td>
        <td>{{ $product->title }}</td>
        <td>{{ $product->price }} BDT</td>
    </tr>
    @endforeach
</table>