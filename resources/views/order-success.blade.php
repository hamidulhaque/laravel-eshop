<x-frontend.layouts.master>
    <div class="bg-light p-5 rounded">
        <div class="card">
            <div class="card-header"></div>
            <div class="card-body">
                <h1>Thank for your orders</h1>
                <a href="{{route('welcome')}}" class="card-link"><- Continue shopping</a>
            </div>
        </div>
    </div>
</x-frontend.layouts.master>